/* Descriptions taken from:
 *  https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkResult.html
 *
 * VK_ERROR_VALIDATION_FAILED_EXT:
 *  https://github.com/KhronosGroup/Vulkan-Docs/commit/7e01ec07847290b54d6d8ad1e8a1d126ab419bb1
 *
 * VK_ERROR_INCOMPATIBLE_VERSION_KHR:
 *  https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/vkGetDeviceAccelerationStructureCompatibilityKHR.html
 *
 * VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT
 *  https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkImageDrmFormatModifierExplicitCreateInfoEXT.html
 *
 * VK_ERROR_NOT_PERMITTED_EXT
 *  https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_global_priority.html
 */

#include <stdio.h>

#include "vulkan_print_result.h"

#define CASE(code, description) case (code):                            \
        fprintf(stderr, "%s: " #code " - " description "\n", message);  \
        return;

int vkResultIsError(VkResult result) {
    return (result < 0);
}

void vkPrintResult(VkResult result, const char *message) {
    switch (result) {
        // Success codes
        CASE(VK_SUCCESS, "Command successfully completed")
        CASE(VK_NOT_READY, "Fence or query not yet completed")
        CASE(VK_TIMEOUT, "Wait operation not completed in the specified time")
        CASE(VK_EVENT_SET, "Event is signaled")
        CASE(VK_EVENT_RESET, "Event is unsignaled")
        CASE(VK_INCOMPLETE, "Return array too small for result")
        CASE(VK_SUBOPTIMAL_KHR,
            "Swapchain no longer matches the surface properties exactly "
            "(but can still be used to present to the surface successfully)")
        CASE(VK_THREAD_IDLE_KHR,
            "Deferred operation not complete but there is no work for this "
            "thread to do at the time of this call")
        CASE(VK_THREAD_DONE_KHR,
            "Deferred operation not complete but there is no work remaining to "
            "assign to additional threads")
        CASE(VK_OPERATION_DEFERRED_KHR,
            "Deferred operation requested and at least some of the work was deferred")
        CASE(VK_OPERATION_NOT_DEFERRED_KHR,
            "Deferred operation requested and no operations were deferred")
        CASE(VK_PIPELINE_COMPILE_REQUIRED_EXT,
            "Requested pipeline creation would have required compilation, "
            "but the application requested compilation to not be performed")
        // Error codes
        CASE(VK_ERROR_OUT_OF_HOST_MEMORY, "Host memory allocation failed")
        CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY, "Device memory allocation failed")
        CASE(VK_ERROR_INITIALIZATION_FAILED,
            "Initialization of an object could not be completed for "
            "implementation-specific reasons")
        CASE(VK_ERROR_DEVICE_LOST, "Logical or physical device was lost")
        CASE(VK_ERROR_MEMORY_MAP_FAILED, "Mapping of a memory object failed")
        CASE(VK_ERROR_LAYER_NOT_PRESENT,
            "Request layer not present or could not be loaded")
        CASE(VK_ERROR_EXTENSION_NOT_PRESENT, "Requested extension is not supported")
        CASE(VK_ERROR_FEATURE_NOT_PRESENT, "Requested feature is not supported")
        CASE(VK_ERROR_INCOMPATIBLE_DRIVER,
            "The requested version of Vulkan is not supported by the driver "
            "or is otherwise incompatible for implementation-specific reasons")
        CASE(VK_ERROR_TOO_MANY_OBJECTS,
            "Too many objects of the type have already been created")
        CASE(VK_ERROR_FORMAT_NOT_SUPPORTED,
            "Requested format not support on this device")
        CASE(VK_ERROR_FRAGMENTED_POOL,
            "Pool allocation failed due to fragmentation of the pool's memory")
        CASE(VK_ERROR_UNKNOWN,
            "Unknown error occurred (either an invalid input was provided "
            "or an implementation failure occured)")
        CASE(VK_ERROR_OUT_OF_POOL_MEMORY, "Pool memory allocation failed")
        CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE,
            "External handle is not a valid handle of the specified type")
        CASE(VK_ERROR_FRAGMENTATION,
            "Descriptor pool creation failed due to fragmentation")
        CASE(VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS,
            "Buffer creation/memory allocation failed because the requested "
            "address is not available; shader group handle assignment failed "
            "because the requested shader group handle information is no longer valid")
        CASE(VK_ERROR_SURFACE_LOST_KHR, "Surface is no longer available")
        CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR,
            "Requested window is already in use by Vulkan or another API "
            "in a manner that prevents it from being used again")
        CASE(VK_ERROR_OUT_OF_DATE_KHR,
            "Surface changed and made incompatible with the swapchain; "
            "further presentation requests using the swapchain will fail")
        CASE(VK_ERROR_INCOMPATIBLE_DISPLAY_KHR,
            "The display used by the swapchain does not use the same presentable image "
            "layout, or is incompatible in a way that prevents sharing an image")
        CASE(VK_ERROR_VALIDATION_FAILED_EXT, "Validation layer testing failed")
        CASE(VK_ERROR_INVALID_SHADER_NV, "One or more shaders failed to compile or link")
        CASE(VK_ERROR_INCOMPATIBLE_VERSION_KHR,
            "Acceleration structure serialized with the specified version "
            "information is not compatible with the specified device")
        CASE(VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT,
            "Validation failed when creating image with the Linux DRM format modifier")
        CASE(VK_ERROR_NOT_PERMITTED_EXT,
            "Insufficient privileges to acquire the specified queue priority")
        CASE(VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT,
            "Operation on a swapchain created with "
            "VK_FULL_SCREEN_EXCLUSIVE_APPLICATION_CONTROLLED_EXT "
            "failed due to not having exclusive full-screen access")
        //VK_ERROR_OUT_OF_POOL_MEMORY_KHR = VK_ERROR_OUT_OF_POOL_MEMORY
        //VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR = VK_ERROR_INVALID_EXTERNAL_HANDLE
        //VK_ERROR_FRAGMENTATION_EXT = VK_ERROR_FRAGMENTATION
        //VK_ERROR_INVALID_DEVICE_ADDRESS_EXT = VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS
        //VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS_KHR = VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS
        //VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT = VK_PIPELINE_COMPILE_REQUIRED_EXT
        default:
            fprintf(stderr, "%s: Unrecognized result code %d\n", message, result);
            return;
    }
}
