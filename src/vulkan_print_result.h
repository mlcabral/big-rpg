#ifndef _VULKAN_PRINT_RESULT_H
#define _VULKAN_PRINT_RESULT_H

#include <vulkan/vulkan_core.h>

#define __TO_STRING(n)   #n
#define _TO_STRING(n)    __TO_STRING(n)

#define vkASSERT(expr, message) do {                                        \
    VkResult _r = (expr);                                                   \
    if (_r != VK_SUCCESS) {                                                 \
        vkPrintResult(_r, __FILE__ ":" _TO_STRING(__LINE__) ": " message);  \
        if (vkResultIsError(_r))                                            \
            throw std::runtime_error(message);                              \
    } } while (0)
// ^ cannot use __PRETTY_FUNCTION__ here

#ifdef __cplusplus
extern "C" {
#endif

int vkResultIsError(VkResult result);
void vkPrintResult(VkResult result, const char *message);

#ifdef __cplusplus
}
#endif

#endif
