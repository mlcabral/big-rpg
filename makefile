## -------------
##  Directories
## -------------
SRC_DIR			:= ./src
HEADER_DIR		:= ./src
OBJ_DIR			:= ./obj
BUILD_DIR		:= ./build
DEBUG_DIR		:= ./debug
SHADER_SUBDIR	:= shaders
ASSETS_SUBDIR	:= assets

SHADER_SRC_DIR 		:= $(SRC_DIR)/$(SHADER_SUBDIR)
SHADER_BUILD_DIR	:= $(BUILD_DIR)/$(SHADER_SUBDIR)
SHADER_DEBUG_DIR	:= $(DEBUG_DIR)/$(SHADER_SUBDIR)
ASSETS_SRC_DIR		:= ./$(ASSETS_SUBDIR)
ASSETS_BUILD_DIR	:= $(BUILD_DIR)/$(ASSETS_SUBDIR)
ASSETS_DEBUG_DIR	:= $(DEBUG_DIR)/$(ASSETS_SUBDIR)

DIRECTORIES := $(OBJ_DIR) $(SHADER_BUILD_DIR) $(SHADER_DEBUG_DIR) $(ASSETS_BUILD_DIR) $(ASSETS_DEBUG_DIR)


## -------
##  Files
## -------
OUTPUT_NAME		:= bigrpg
OUTPUT			:= $(BUILD_DIR)/$(OUTPUT_NAME)
OUTPUT_DEBUG	:= $(DEBUG_DIR)/$(OUTPUT_NAME)
SRCS			:= $(wildcard $(SRC_DIR)/*.c*)
# source files starting with "." are automatically excluded
_SRCS_TMP		:= $(patsubst %.cpp,%.c,$(SRCS))
DEPS			:= $(patsubst %.c,%.d,$(_SRCS_TMP))
OBJS			:= $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(_SRCS_TMP))
DEBUG_OBJS		:= $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%_debug.o,$(_SRCS_TMP))
SHADERS_SRC		:= $(wildcard $(SHADER_SRC_DIR)/*.glsl)
SHADERS_BUILD	:= $(patsubst $(SHADER_SRC_DIR)/%.glsl,$(SHADER_BUILD_DIR)/%.spv,$(SHADERS_SRC))
SHADERS_DEBUG	:= $(patsubst $(SHADER_SRC_DIR)/%.glsl,$(SHADER_DEBUG_DIR)/%.spv,$(SHADERS_SRC))


## -------
##  Flags
## -------
OPT_LEVEL	:= -O2
DEBUG_FLAGS	:= -g3 -DDEBUG
FLAGS		:= -Wall -Wpedantic -Wextra
CFLAGS		:= -std=c17 $(FLAGS)
CXXFLAGS	:= -std=c++17 $(FLAGS)
LDLIBS		:= `pkg-config --static --libs glfw3` -lvulkan
DEPFLAGS	:= -MMD -MP


## --------
##  Macros
## --------
begin-msg	:= @echo -e "\e[1;37;44m::
end-msg		:= \e[0m"

# Functions
msg			= $(begin-msg) $(1) $(end-msg)
yellow		= \e[33m$(1)\e[37m
msg-debug	= $(begin-msg) $(call yellow,[Debug]) $(1) $(end-msg)

define compile-msg
$(call msg,Compiling $@)
endef

define compile-msg-debug
$(call msg-debug,Compiling $@)
endef

define compile-args
$(LDLIBS) $(DEPFLAGS) -c -o $@ $<
endef

define link-objs
$(call msg,Linking objects)
g++ $(CXXFLAGS) $(OPT_LEVEL) $(LDLIBS) $^ -o $(OUTPUT)
endef

define link-objs-debug
$(call msg-debug,Linking objects)
g++ $(CXXFLAGS) $(DEBUG_FLAGS) $(LDLIBS) $^ -o $(OUTPUT_DEBUG)
endef

define compile-c++
$(compile-msg)
g++ $(CXXFLAGS) $(OPT_LEVEL) $(compile-args)
endef

define compile-c
$(compile-msg)
gcc $(CFLAGS) $(OPT_LEVEL) $(compile-args)
endef

define compile-c++-debug
$(compile-msg-debug)
g++ $(CXXFLAGS) $(DEBUG_FLAGS) $(compile-args)
endef

define compile-c-debug
$(compile-msg-debug)
gcc $(CFLAGS) $(DEBUG_FLAGS) $(compile-args)
endef

define compile-spirv
$(compile-msg)
glslangValidator -V -o $@ $^
endef


## -------
##  Rules
## -------
.PHONY: all clean

release: directories shaders build assets

debug: directories shaders_debug _debug assets

all: release debug

directories: | $(DIRECTORIES)

$(DIRECTORIES):
	$(call msg,Creating directory $(call yellow,$@))
	@mkdir -p $@

_debug: $(DEBUG_OBJS)
	$(link-objs-debug)

build: $(OBJS)
	$(link-objs)

shaders: $(SHADERS_BUILD)

shaders_debug: $(SHADERS_DEBUG)

assets: $(ASSETS_BUILD_DIR)/noise.png $(ASSETS_DEBUG_DIR)/noise.png

%noise.png: gen-rnd-img.sh
	$(call msg,Generating $@)
	@./gen-rnd-img.sh $@

-include $(DEPS)

# Release
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(compile-c++)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(compile-c)

# Debug
$(OBJ_DIR)/%_debug.o: $(SRC_DIR)/%.cpp
	$(compile-c++-debug)

$(OBJ_DIR)/%_debug.o: $(SRC_DIR)/%.c
	$(compile-c-debug)

# Shaders
$(SHADER_BUILD_DIR)/%.spv: $(SHADER_SRC_DIR)/%.glsl
	$(compile-spirv)

$(SHADER_DEBUG_DIR)/%.spv: $(SHADER_SRC_DIR)/%.glsl
	$(compile-spirv)

clean:
	rm -f $(OBJ_DIR)/* $(SHADERS_BUILD) $(OUTPUT) $(SHADERS_DEBUG) $(OUTPUT_DEBUG) $(ASSETS_BUILD_DIR)/* $(ASSETS_DEBUG_DIR)/*
