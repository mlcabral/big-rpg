# Something In Vulkan (~It'll Be Cool, I Promise~)

Just a project I started so I could learn Vulkan. The original objective was to make a whole videogame but I decided to aim for something simpler for now, and then possibly expand on it. This is the current plan:

1. [ ] Basic renderer for LDraw (LEGO) models
2. [ ] Make that renderer fancier 
3. [ ] Add tools to animate the models
4. [ ] Add a physics engine
5. [ ] Add a scripting engine? (gotta look deeper into the topic first)
6. [ ] ...game???

Small note: the *_autocomplete_hack.h files are just files I keep open in Geany so I can have autocomplete for Vulkan and GLFW symbols.


## Required packages for development

Arch repos:

```
vulkan-headers vulkan-validation-layers vulkan-tools glm glfw-x11
```

AUR:

```
stb
```
