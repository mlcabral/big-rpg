#!/usr/bin/env bash

# very comp engineer-y script name /shrug

# default resolution
resolution="512x512"
output="out.png"

[[ $1 =~ "help" || $1 =~ "-h" ]] && {
    echo "Usage: $0 [<width>x<height>] [<output name>]"
    exit 0
}

[[ -n $1 ]] && {    # skip parsing if no arguments were given (duh)
    if [[ -n $2 ]]
    then
        # $0 <width>x<height> <output name>
        output=$2
        [[ $1 =~ ^[[:digit:]]+x[[:digit:]]+$ ]] && resolution=${BASH_REMATCH[0]}
    else
        if [[ $1 =~ ^[[:digit:]]+x[[:digit:]]+$ ]]
        then
            # $0 <width>x<height>
            resolution=${BASH_REMATCH[0]}
        else
            # $0 <output name>
            output=$1
        fi
    fi
}

echo "Generated ${resolution} noise image: ${output}"
magick -size $resolution xc: +noise Random ${output}
